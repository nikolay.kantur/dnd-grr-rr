# Публичная задача
У меня есть 3 STL модельки варвара с разным оружием, сам варвар совпадает, отличается только оружие, и ещё есть отдельно платформа.

Необходимо сделать 5 моделей с соединением по типу Лего (кисть, стопы)
- фигурка варвара, но без кисти
- 3 вида оружия вместе с кистью
- платформа

Фиксы
- меч "впаян" в тело, его надо повернуть в руке, что бы его можно было распечатать отдельно от тела (отсутствующую часть рога, можно отзеркалить с другой стороны)
- заменить "дно" платформы и добавить надписи кастомным шрифтом https://dl.dafont.com/dl/?f=dungeon_sn:
  - 27.03.2022
  - Nikolay Kantur
  - Gordon Grr’gr



![screenshot](screenshots/barb-sword.png)
![screenshot](screenshots/barb-axe.png)
![screenshot](screenshots/barb-speare.png)

![screenshot](screenshots/barb-hand-cut.png)
![screenshot](screenshots/barb-sword-problem.png)
![screenshot](screenshots/bottom.png)

![screenshot](screenshots/barb-all.png)






  
